#!/bin/bash

docker stop $(docker ps --format '{{.Names}}' | grep node-web-app)
docker pull kaluspresence/node-web-app:latest
docker run -p 8080:8080 -d kaluspresence/node-web-app:latest
